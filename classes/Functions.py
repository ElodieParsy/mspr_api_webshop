import sys, os, json, datetime, requests,smtplib, ssl,yaml
import urllib3
urllib3.disable_warnings()
from flask import abort
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

##########################################################################
#  Script contenant les fonctions 
##########################################################################


class Functions: 
    def config_yaml(self):
        # Ouverture et lecture du fichier de configuration YAML
        try:
            _f = open('config/config.yaml', 'r')
            config = yaml.load(_f, Loader=yaml.FullLoader)
        except Exception as e:
          print("Impossible d'ouvrir le fichier" + str(e))
        return config

    def objectToString(self,element):
        return json.dumps(element,default=self.jconverter,indent=4, sort_keys=True)

    def jconverter(self, o):
        if isinstance(o, datetime.datetime):
            return o.__str__()  


    

 

    
    


