import pytest,os,datetime
from Functions import Functions
from Router import Router
from main import app
import requests, urllib3
urllib3.disable_warnings()

@pytest.fixture
def functions():
    return Functions()

#
# Test qui vérifie que le fichier yaml s'ouvre correctement et contient les champs attendus
#
def test_config_yaml_success(functions):
    config = functions.config_yaml()
    assert isinstance(config, dict)
    assert "normal" in config
    assert "test" in config   

#
# Test qui vérifie que la fonction renvoie la chaîne JSON attendue pour chaque entrée,
# et que la fonction jconverter est correctement appelée pour que l'objet datetime soit converti en chaîne.
#
def test_objectToString(functions): 
    input_dict = {"key": datetime.datetime(2022, 2, 24, 12, 0)}
    expected_output = '{\n    "key": "2022-02-24 12:00:00"\n}'
    assert functions.objectToString(input_dict) == expected_output

    # Test dictionnaire contenant des strings
    input_dict = {"key": "value"}
    expected_output = '{\n    "key": "value"\n}'
    assert functions.objectToString(input_dict) == expected_output

    # Test avec un dictionnaire contenant une liste
    input_dict = {"key": [1, 2, 3]}
    expected_output = '{\n    "key": [\n        1,\n        2,\n        3\n    ]\n}'
    assert functions.objectToString(input_dict) == expected_output