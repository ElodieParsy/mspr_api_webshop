import os, sys, datetime,json,hashlib,mysql.connector, secrets, requests
import requests, urllib3
urllib3.disable_warnings()
from datetime import timedelta
from flask import Flask,request,Response,abort,session
from flaskext.mysql import MySQL
from threading import Timer

if hasattr(sys, '_called_from_test'):
    from Functions import Functions
else:
    from classes.Functions import Functions

class Router: 


    def __init__(self,app=None,db=None,functions=None):

        self.app = app        
        self.functions= functions
        self.db = db
        self.secret = secrets.SystemRandom()

        """Bloc exécuté aaprès le traitement de chaque requête Http
        -> Modification des entêtes de la réponse pour autoriser les requêtes cross-domaines (CORS) + traçage des requêtes.
        """                                      
        @app.after_request
        def afterEveryRequest(response):  
            # Modification des Headers pour autoriser les attributs spéciaux
            headers = request.headers.get('Access-Control-Request-Headers')
            if headers:
                response.headers['Access-Control-Allow-Headers'] = headers
            
            response.headers['Access-Control-Allow-Origin'] = request.environ.get('HTTP_ORIGIN')                
            response.headers['Access-Control-Allow-Credentials'] = 'true'        
            response.headers['Access-Control-Allow-Methods'] = 'GET, HEAD, POST, OPTIONS, PUT, PATCH, DELETE'  
            return response


        # Vérifie avant chaque requête sauf pour la route de login et la méthode "options"
        # que le token api est présent dans le header de la réponse
        @app.before_request
        def check_token():  
            if "/swagger" not in request.full_path and request.path != "/api/webshop/login" and request.method != "OPTIONS":
                if 'x-access-token' not in request.headers:
                    abort(401, "Authentification requise.")
                


    """
        Définitions des routes 

    """
    def register_routes(self):
        app = self.app    
        db = self.db['cursor']
        db_connection = self.db['connection']
        _ = self.functions

     
        
        #
        # Route de login (mail,password)
        #
        @app.route('/api/webshop/login', methods=['POST'])
        def login():
            # Récupération des paramètres de la requête
            try:
                data = json.loads(request.data)
            except Exception as e:
                print(e)
                abort(400, "Les paramètres doivent être au format json")
            
            # on hash le password
            secret = hashlib.sha512(
                data['password'].encode('utf-8'), # Convert the password 
                ).hexdigest()
            
            # Requête pour vérifier si les identifiants saisis correspondent à un client du webshop
            sql = "SELECT id FROM UtilisateurWebshop WHERE mail =%s and password =%s;"
            val = (data["mail"],secret)
            db.execute(sql,val)
            user = db.fetchone()

            if user is None:
                abort(400, "Identifiants incorrects.")
            else:
                # Démarrage de la session 
                session["id"] = user[0]

                # Récupération du token 
                configuration = _.config_yaml()
                token = configuration["token"]["token_api_webshop"]
 
            return {'token': token},200

        
        #
        # Récupère pour chaque produit : (id,nom, description,prix,couleur,stock)
        #
        @app.route('/api/webshop/products', methods=['GET'])
        def get_products():
            data = []
            try:
               r = requests.get('https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products',verify=False)
               products = r.json()
            except Exception as e:
                print("Impossible de récupérer la liste des produits " + str(e))  
                abort(400)
            
            for product in products:
                data.append({
                    "id":product["id"],
                    "name":product["name"],
                    "description":product["details"]["description"],
                    "price":product["details"]["price"],
                    "color":product["details"]["color"],
                    "stock":product["stock"]
                })

            return _.objectToString(data),200


        #
        # Récupère un produit spécifique : (id,nom, description,prix,couleur,stock)
        # @param = idProduct
        #
        @app.route('/api/webshop/product/<idProduct>', methods=['GET'])
        def get_product(idProduct):
            data = {}
            try:
               r = requests.get('https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products/'+str(idProduct),verify=False)
               product = r.json()
            except Exception as e:
                print("Impossible de récupérer le produit " + str(e))  
                abort(400)
            
            data = {
                "id":product["id"],
                "name":product["name"],
                "description":product["details"]["description"],
                "price":product["details"]["price"],
                "color":product["details"]["color"],
                "stock":product["stock"]
            }
           
            return _.objectToString(data),200


        
        #
        # Récupère pour chaque client : (id,nom,prénom,adresse,entreprise,une liste de ses commandes (idClient,idCommande,date))
        #
        @app.route('/api/webshop/customers', methods=['GET'])
        def get_customers():
            data=[]
            try:
               r = requests.get('https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers',verify=False)
               customers = r.json()
            except Exception as e:
                print("Impossible de récupérer les clients " + str(e))  
                abort(400)

            for customer in customers:
                data.append({
                    "id": customer["id"],
                    "nom": customer["lastName"],
                    "prenom": customer["firstName"],
                    "adresse": customer["address"]["postalCode"] +" "+ customer["address"]["city"],
                    "entreprise": customer["company"]["companyName"],
                    "orders":customer["orders"]
                })
                
            return _.objectToString(data),200


        #
        # Récupère pour un client spécifique : (id,nom,prénom,adresse,entreprise,une liste de ses commandes (idClient,idCommande,date))
        #
        @app.route('/api/webshop/customer/<idC>', methods=['GET'])
        def get_customer(idC):
            data={}
            try:
               t = 'https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/'+str(idC)
               r = requests.get('https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/'+str(idC),verify=False)
               customer = r.json()
            except Exception as e:
                print(t)
                print("Impossible de récupérer le client " + str(e))  
                abort(400)

            data = {
                "id": customer["id"],
                "nom": customer["lastName"],
                "prenom": customer["firstName"],
                "adresse": customer["address"]["postalCode"] +" "+ customer["address"]["city"],
                "entreprise": customer["company"]["companyName"],
                "orders": customer["orders"]
            }
           
            return _.objectToString(data),200


        #
        # Récupère la liste des commandes d'un client : (idClient,idCommande,date)
        #
        @app.route('/api/webshop/customer/<idCustomer>/orders', methods=['GET'])
        def get_orders(idCustomer):
            data=[]
            try:
               r = requests.get('https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/'+str(idCustomer)+'/orders',verify=False)
               orders = r.json()
            except Exception as e:
                print("Impossible de récupérer la liste des commandes du client " + str(e))  
                abort(400)

            for order in orders:
                data.append(order)
            return _.objectToString(data),200

        
        #
        # Récupère les produits d'une commande : (idClient,idCommande,nom, description,prix,couleur,stock)
        #
        @app.route('/api/webshop/customer/<idCustomer>/order/products/<idOrder>/products', methods=['GET'])
        def get_order_products(idCustomer,idOrder):
            data = {}
            data=[]
            try:
               r = requests.get('https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/'+str(idCustomer)+'/orders/'+str(idOrder)+'/products',verify=False)
               order = r.json()
            except Exception as e:
                print("Impossible de récupérer la liste des commandes du client " + str(e))  
                abort(400)

            for product in order:
                data.append({
                    "idClient":product["id"],
                    "idCommande":product["orderId"],
                    "name":product["name"],
                    "description":product["details"]["description"],
                    "price":product["details"]["price"],
                    "color":product["details"]["color"],
                    "stock":product["stock"],
                    "dateCreation":product["createdAt"]
                })
                
            return _.objectToString(data),200
            

       


        




    