from main import app
from Functions import Functions
from Router import Router
import json,hashlib,pytest



@pytest.fixture
def mockData():
    data = {
        "mail": "test_user@example.com",
        "password": "new_password",
        "idC":8,
        "idOrder":8,
        "idProduct":2,
        "token": "QanBwAp45xZeG8Hdy_YJzA"
        
    }
    return data

class TestApiRoutes:

    #
    # Test sur la route /api/webshop/login
    #
    def test_login(self,mockData):
        # Test le statut de la requête POST
        response = app.test_client().post('/api/webshop/login', data=json.dumps(mockData), content_type='application/json')
        assert response.status_code == 200
        
    #
    # Test sur la route /api/webshop/products
    #   
    def test_products(self,mockData):
        headers = {'x-access-token': mockData["token"]}
        # Test le statut de la requête get
        response = app.test_client().get('/api/webshop/products', headers=headers, content_type='application/json')
        assert response.status_code == 200

    #
    # Test sur la route /api/webshop/product/<id>
    #
    def test_specific_product(self,mockData):
        headers = {'x-access-token': mockData["token"]}
        # Test le statut de la requête get
        response = app.test_client().get('/api/webshop/product/'+str(mockData["idProduct"]), headers=headers, content_type='application/json')
        assert response.status_code == 200

    #
    # Test sur la route /api/webshop/customers
    #
    def test_customers(self,mockData):
        headers = {'x-access-token': mockData["token"]}
        # Test le statut de la requête get
        response = app.test_client().get('/api/webshop/customers', headers=headers, content_type='application/json')
        assert response.status_code == 200

    #
    # Test sur la route /api/webshop/customer/<idCustomer>
    #
    def test_specific_customer(self,mockData):
        headers = {'x-access-token': mockData["token"]}
        # Test le statut de la requête get
        response = app.test_client().get('/api/webshop/customer/'+str(mockData["idC"]), headers=headers, content_type='application/json')
        assert response.status_code == 200  
    
    #
    # Test sur la route /api/webshop/customer/<idCustomer>/orders
    #
    def test_customer_orders(self,mockData):
        headers = {'x-access-token': mockData["token"]}
        # Test le statut de la requête get
        response = app.test_client().get('/api/webshop/customer/'+str(mockData["idC"])+'/orders', headers=headers, content_type='application/json')
        assert response.status_code == 200 

    #
    # Test sur la route /api/webshop/customer/<idCustomer>/order/products/<idOrder>/products
    #
    def test_customer_orders_products(self,mockData):
        headers = {'x-access-token': mockData["token"]}
        # Test le statut de la requête get
        response = app.test_client().get('/api/webshop/customer/'+str(mockData["idC"])+'/order/products/'+str(mockData["idOrder"])+'/products', headers=headers, content_type='application/json')
        assert response.status_code == 200 
    