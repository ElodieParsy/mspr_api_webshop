# Utilisez l'image de base python
FROM python:3.9-slim-buster

# Définit le répertoire de travail à /app
WORKDIR /app

# Copie les fichiers du projet dans le conteneur
COPY . .

# Installe les dépendances du projet
RUN pip install --no-cache-dir -r requirements.txt

# Expose le port 5000 utilisé par l'application
EXPOSE 5000

# Démarre l'application lorsque le conteneur est lancé
CMD ["python", "main.py"]
